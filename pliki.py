import csv
import json
import pickle
import sys

class FileReader:
    def __init__(self, input_file):
        self.input_file = input_file
        self.data = None

    def read(self):
        _, extension = self.input_file.split('.')
        if extension == 'csv':
            self.data = self.read_csv()
        elif extension == 'json':
            self.data = self.read_json()
        elif extension == 'txt':
            self.data = self.read_txt()
        elif extension == 'pickle':
            self.data = self.read_pickle()
        else:
            print("Nieobsługiwany format pliku")
            sys.exit(1)

    def read_csv(self):
        with open(self.input_file, 'r', newline='') as file:
            reader = csv.reader(file)
            return [row for row in reader]

    def read_json(self):
        with open(self.input_file, 'r') as file:
            return json.load(file)

    def read_txt(self):
        with open(self.input_file, 'r') as file:
            return [line.strip() for line in file]

    def read_pickle(self):
        with open(self.input_file, 'rb') as file:
            return pickle.load(file)


class FileWriter:
    def __init__(self, output_file, data):
        self.output_file = output_file
        self.data = data

    def write(self):
        _, extension = self.output_file.split('.')
        if extension == 'csv':
            self.write_csv()
        elif extension == 'json':
            self.write_json()
        elif extension == 'txt':
            self.write_txt()
        elif extension == 'pickle':
            self.write_pickle()
        else:
            print("Nieobsługiwany format pliku")
            sys.exit(1)

    def write_csv(self):
        with open(self.output_file, 'w', newline='') as file:
            writer = csv.writer(file)
            writer.writerows(self.data)

    def write_json(self):
        with open(self.output_file, 'w') as file:
            json.dump(self.data, file, indent=4)

    def write_txt(self):
        with open(self.output_file, 'w') as file:
            for line in self.data:
                file.write(f"{line}\n")

    def write_pickle(self):
        with open(self.output_file, 'wb') as file:
            pickle.dump(self.data, file)


class DataModifier:
    def __init__(self, data, modifications):
        self.data = data
        self.modifications = modifications

    def apply_modifications(self):
        for mod in self.modifications:
            x, y, value = map(str.strip, mod.split(','))
            x, y = int(x), int(y)
            self.data[y][x] = value


if __name__ == "__main__":
    if len(sys.argv) < 4:
        print("Użycie: python reader.py <plik_wejściowy> <plik_wyjściowy> <zmiana_1> <zmiana_2> ... <zmiana_n>")
        sys.exit(1)

    input_file = sys.argv[1]
    output_file = sys.argv[2]
    modifications = sys.argv[3:]

    reader = FileReader(input_file)
    reader.read()

    modifier = DataModifier(reader.data, modifications)
    modifier.apply_modifications()

    writer = FileWriter(output_file, modifier.data)
    writer.write()

    print("Zawartość zmodyfikowanego pliku:")
    for row in modifier.data:
        print(row)